package com.artal.animation.helper;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AnimationHelper extends Game {
	public static AssetManager manager;
	private SpriteBatch batch;

	private MainScreen mainScreen;

	public AnimationHelper() {
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		mainScreen = new MainScreen(this, batch );
		setScreen(mainScreen);
	}
}
