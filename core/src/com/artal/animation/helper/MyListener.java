package com.artal.animation.helper;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

/**
 * Created by Artem on 13.07.2015.
 */
public class MyListener extends InputListener {
    private MainScreen screen;

    public MyListener(MainScreen screen) {
        this.screen = screen;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        screen.addPoint(x,y);

        return true;
    }
}
