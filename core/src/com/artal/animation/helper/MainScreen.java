package com.artal.animation.helper;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.awt.Button;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Artem on 13.07.2015.
 */
public class MainScreen implements Screen {

    private AnimationHelper refer;
    private SpriteBatch batch;
    private Stage mainStage;

    private TextureAtlas atlas;
    private TextureRegion texture;

    private BitmapFont font;

    private Array<Vector2> points;
    private boolean isEscapePressed;
    private boolean isSpacePressed;

    private int animationFrameIndex;
    private String name;

    public MainScreen(AnimationHelper refer, SpriteBatch batch) {
        this.refer = refer;
        this.batch = batch;

        font = new BitmapFont();

        init();


        points = new Array<Vector2>();

        mainStage = new Stage(new StretchViewport(960, 540), batch);
        mainStage.addListener(new MyListener(this));
    }

    private void init() {
        animationFrameIndex = 0;
        atlas = getAtlas("data/fishesAtlas");
        texture = atlas.findRegion("Shark", 3);
        name = "//  Shark _____ 3";
    }

    private TextureAtlas getAtlas(String name) {
        AnimationHelper.manager = new AssetManager();
        name = name.concat(".txt");
        AnimationHelper.manager.load(name, TextureAtlas.class);
        AnimationHelper.manager.finishLoading();
        TextureAtlas atlas = AnimationHelper.manager.get(name, TextureAtlas.class);

        return atlas;
    }


    @Override
    public void render(float delta) {
        Gdx.gl20.glClearColor(1, 1, 1, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.input.setInputProcessor(mainStage);

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE) && !isEscapePressed) {
            isEscapePressed = true;
            points.removeIndex(points.size - 1);
        } else if (!Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            isEscapePressed = false;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && !isSpacePressed) {
            isSpacePressed = true;
            writePointToFile();
        } else if (!Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            isSpacePressed = false;
        }

        draw(delta);
    }

    private void writePointToFile() {
        String str = createOutputString();

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("anima.txt"));
            writer.append(str);
        } catch (IOException e) {
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
            }
        }
    }

    private String createOutputString() {
        String str = new String();

        str = name + '\n';

        if (animationFrameIndex == 0) {
            for (int i = 0; i < points.size; i++) {
                str = str.concat("pointArray.add(new Array<Vector2>());" + '\n');
            }
        }

        for (int i = 0; i < points.size; i++) {
            str = str.concat("pointArray.get(").concat(Integer.toString(animationFrameIndex)).
                    concat(").add(new Vector2(").concat(Integer.toString((int) points.get(i).x)).
                    concat(", ").concat(Float.toString(points.get(i).y)).concat("));" + '\n');
        }

        str += '\n';
        return str;
    }

    private void draw(float delta) {
        mainStage.act(delta);
        mainStage.draw();

        batch.begin();

        batch.draw(texture, 0, 0);

        for (int i = 0; i < points.size; i++) {
            font.draw(batch, "O", points.get(i).x, points.get(i).y);
        }

        batch.end();
    }

    public void addPoint(float x, float y) {
        points.add(new Vector2(x, y));
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


}
